# Colorized atlas commands

## Description

This [Oh-My-Zsh](http://ohmyz.sh) plugin adds color highlighting and a few useful aliases to a selection of the [atlas commands](https://developer.atlassian.com/server/framework/atlassian-sdk/command-reference/) from the [Atlassian Developer SDK](https://developer.atlassian.com/server/framework/atlassian-sdk/).

The following commands are supported:
`atlas-clean`, `atlas-compile`, `atlas-debug`, `atlas-integration-test`, `atlas-mvn`, `atlas-package`, `atlas-run`, `atlas-run-standalone`, `atlas-unit-test`

Overall it was inspired by the wonderful [mvn plugin](https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/mvn) for Oh-My-Zsh.

## Aliases

| Alias      | Command                                   |
|:-----------|:------------------------------------------|
| `atlas-dl` | `atlas-debug --server localhost`          |
| `atlas-it` | `atlas-integration-test`                  |
| `atlas-rl` | `atlas-run --server localhost`            |
| `atlas-rsl`| `atlas-run-standalone --server localhost` |
| `atlas-ut` | `atlas-unit-test`                         |

## Installation

This plugin is not part of the [official Oh-My-Zsh plugin list](https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins), so you have to install it manually:

First, edit your `~/.zshrc`file and add `colorized-atlas-cmd` to the list of the enabled plugins:
```
plugins=( ... colorized-atlas-cmd )
```

Switch to the Oh-My-Zsh custom plugin directory and clone this repository:
```
cd ~/.oh-my-zsh/custom/plugins
git clone https://bitbucket.org/syracom_dev/colorized-atlas-cmd.git colorized-atlas-cmd
```

Restart your shell and call the atlas commands as normal.

## Acknowledgements

Copyright (c) 2018 syracom AG

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.