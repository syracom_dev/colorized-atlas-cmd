BOLD=$(tput bold)
TEXT_RED=$(tput setaf 1)
TEXT_GREEN=$(tput setaf 2)
TEXT_YELLOW=$(tput setaf 3)
TEXT_BLUE=$(tput setaf 4)
TEXT_MAGENTA=$(tput setaf 5)
TEXT_CYAN=$(tput setaf 6)
TEXT_WHITE=$(tput setaf 7)
RESET_FORMATTING=$(tput sgr0)

INFO_HIGHLIGHT="s/\(\[INFO\]\)\(.*\)/${TEXT_WHITE}${BOLD}\1${RESET_FORMATTING}\2/g"
BUILD_SUCCESSFUL="s/\(.*\[INFO\].*\)\(BUILD SUCCESS\)/\1${TEXT_GREEN}${BOLD}\2${RESET_FORMATTING}/g"
BUILD_ERROR="s/\(.*\[INFO\].*\)\(BUILD FAILURE\)/\1${TEXT_RED}${BOLD}\2${RESET_FORMATTING}/g"
WARNING_HIGHLIGHT="s/\(\[WARNING\].*\)/${TEXT_YELLOW}${BOLD}\1${RESET_FORMATTING}/g"
ERROR_HIGHLIGHT="s/\(\[ERROR\].*\)/${TEXT_RED}${BOLD}\1${RESET_FORMATTING}/g"
STARTED_HIGHLIGHT="s/\(.*\[INFO\][^ ]* \)\(.* started successfully.*\)/\1${TEXT_MAGENTA}${BOLD}\2${RESET_FORMATTING}/g"
MAVEN_PLUGIN_HIGHLIGHT="s/\(.*\[INFO\][^ ]* \)\(--- .* ---.*\)/\1${TEXT_WHITE}${BOLD}\2${RESET_FORMATTING}/g"
EMBEDDED_WARN_HIGHLIGHT="s/\(.*\[talledLocalContainer\] .*\)\( WARN .*\)/\1${TEXT_YELLOW}\2${RESET_FORMATTING}/g"
EMBEDDED_DEBUG_HIGHLIGHT="s/\(.*\[talledLocalContainer\] .*\)\( DEBUG .*\)/\1${TEXT_CYAN}\2${RESET_FORMATTING}/g"
EMBEDDED_ERROR_HIGHLIGHT="s/\(.*\[talledLocalContainer\] .*\)\( ERROR .*\)/\1${TEXT_RED}\2${RESET_FORMATTING}/g"
                      
atlas-clean-color() {
  (
    unset LANG
    LC_CTYPE=C atlas-clean "$@" | sed -e "${BUILD_SUCCESSFUL}" -e "${BUILD_ERROR}" -e "${INFO_HIGHLIGHT}" -e "${WARNING_HIGHLIGHT}" -e "${ERROR_HIGHLIGHT}" -e "${MAVEN_PLUGIN_HIGHLIGHT}"
    echo -ne "${RESET_FORMATTING}"
  )
}

atlas-compile-color() {
  (
    unset LANG
    LC_CTYPE=C atlas-compile "$@" | sed -e "${BUILD_SUCCESSFUL}" -e "${BUILD_ERROR}" -e "${INFO_HIGHLIGHT}" -e "${WARNING_HIGHLIGHT}" -e "${ERROR_HIGHLIGHT}" -e "${MAVEN_PLUGIN_HIGHLIGHT}"
    echo -ne "${RESET_FORMATTING}"
  )
}

atlas-package-color() {
  (
    unset LANG
    LC_CTYPE=C atlas-package "$@" | sed -e "${BUILD_SUCCESSFUL}" -e "${BUILD_ERROR}" -e "${INFO_HIGHLIGHT}" -e "${WARNING_HIGHLIGHT}" -e "${ERROR_HIGHLIGHT}" -e "${MAVEN_PLUGIN_HIGHLIGHT}"
    echo -ne "${RESET_FORMATTING}"
  )
}

atlas-debug-color() {
  (
    unset LANG
    LC_CTYPE=C atlas-debug "$@" | sed -e "${BUILD_SUCCESSFUL}" -e "${BUILD_ERROR}" -e "${INFO_HIGHLIGHT}" -e "${WARNING_HIGHLIGHT}" -e "${ERROR_HIGHLIGHT}" -e "${STARTED_HIGHLIGHT}" -e "${MAVEN_PLUGIN_HIGHLIGHT}" -e "${EMBEDDED_WARN_HIGHLIGHT}" -e "${EMBEDDED_DEBUG_HIGHLIGHT}" -e "${EMBEDDED_ERROR_HIGHLIGHT}"
    echo -ne "${RESET_FORMATTING}"
  )
}

atlas-run-color() {
  (
    unset LANG
    LC_CTYPE=C atlas-run "$@" | sed -e "${BUILD_SUCCESSFUL}" -e "${BUILD_ERROR}" -e "${INFO_HIGHLIGHT}" -e "${WARNING_HIGHLIGHT}" -e "${ERROR_HIGHLIGHT}" -e "${STARTED_HIGHLIGHT}" -e "${MAVEN_PLUGIN_HIGHLIGHT}" -e "${EMBEDDED_WARN_HIGHLIGHT}" -e "${EMBEDDED_DEBUG_HIGHLIGHT}" -e "${EMBEDDED_ERROR_HIGHLIGHT}"
    echo -ne "${RESET_FORMATTING}"
  )
}

atlas-run-standalone-color() {
  (
    unset LANG
    LC_CTYPE=C atlas-run-standalone "$@" | sed -e "${BUILD_SUCCESSFUL}" -e "${BUILD_ERROR}" -e "${INFO_HIGHLIGHT}" -e "${WARNING_HIGHLIGHT}" -e "${ERROR_HIGHLIGHT}" -e "${STARTED_HIGHLIGHT}" -e "${MAVEN_PLUGIN_HIGHLIGHT}" -e "${EMBEDDED_WARN_HIGHLIGHT}" -e "${EMBEDDED_DEBUG_HIGHLIGHT}" -e "${EMBEDDED_ERROR_HIGHLIGHT}"
    echo -ne "${RESET_FORMATTING}"
  )
}

atlas-mvn-color() {
  (
    unset LANG
    LC_CTYPE=C atlas-mvn "$@" | sed -e "${BUILD_SUCCESSFUL}" -e "${BUILD_ERROR}" -e "${INFO_HIGHLIGHT}" -e "${WARNING_HIGHLIGHT}" -e "${ERROR_HIGHLIGHT}" -e "${STARTED_HIGHLIGHT}" -e "${MAVEN_PLUGIN_HIGHLIGHT}" -e "${EMBEDDED_WARN_HIGHLIGHT}" -e "${EMBEDDED_DEBUG_HIGHLIGHT}" -e "${EMBEDDED_ERROR_HIGHLIGHT}"
    echo -ne "${RESET_FORMATTING}"
  )
}

atlas-integration-test-color() {
  (
    unset LANG
    LC_CTYPE=C atlas-integration-test "$@" | sed -e "${BUILD_SUCCESSFUL}" -e "${BUILD_ERROR}" -e "${INFO_HIGHLIGHT}" -e "${WARNING_HIGHLIGHT}" -e "${ERROR_HIGHLIGHT}" -e "${STARTED_HIGHLIGHT}" -e "${MAVEN_PLUGIN_HIGHLIGHT}" -e "${EMBEDDED_WARN_HIGHLIGHT}" -e "${EMBEDDED_DEBUG_HIGHLIGHT}" -e "${EMBEDDED_ERROR_HIGHLIGHT}"
    echo -ne "${RESET_FORMATTING}"
  )
}

atlas-unit-test-color() {
  (
    unset LANG
    LC_CTYPE=C atlas-unit-test "$@" | sed -e "${BUILD_SUCCESSFUL}" -e "${BUILD_ERROR}" -e "${INFO_HIGHLIGHT}" -e "${WARNING_HIGHLIGHT}" -e "${ERROR_HIGHLIGHT}" -e "${STARTED_HIGHLIGHT}" -e "${MAVEN_PLUGIN_HIGHLIGHT}" -e "${EMBEDDED_WARN_HIGHLIGHT}" -e "${EMBEDDED_DEBUG_HIGHLIGHT}" -e "${EMBEDDED_ERROR_HIGHLIGHT}"
    echo -ne "${RESET_FORMATTING}"
  )
}

alias atlas-clean="atlas-clean-color"
alias atlas-package="atlas-package-color"
alias atlas-run="atlas-run-color"
alias atlas-debug="atlas-debug-color"
alias atlas-run-standalone="atlas-run-standalone-color"
alias atlas-rl="atlas-run --server localhost"
alias atlas-rsl="atlas-run-standalone --server localhost"
alias atlas-debug="atlas-debug --server localhost"
alias atlas-mvn="atlas-mvn-color"
alias atlas-integration-test="atlas-integration-test-color"
alias atlas-it="atlas-integration-test"
alias atlas-unit-test="atlas-unit-test-color"
alias atlas-ut="atlas-unit-test"
